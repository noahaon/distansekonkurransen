# pylint: disable=E1101
from requests_oauthlib import OAuth2Session
from flask_sqlalchemy import SQLAlchemy
from flask.json import jsonify
from flask import Flask, redirect, render_template, request, session, url_for
from datetime import datetime
import time
import os
import json
KULL1 = '73'
KULL2 = '72'
KULL3 = '71'

STARTDATE_DYNAMISK = '2020-05-01T12:00:00Z' #Format: YYY-MM-DD"T"hh:mm:ss"Z". Endre denne for å sette startdato
STARTDATE = datetime.strptime(STARTDATE_DYNAMISK, '%Y-%m-%dT%H:%M:%SZ').date() # Parser datoen til å bli et python datetime-
# objekt. Ikke endre

with open('/etc/config.json') as config_file:
  config = json.load(config_file)

app = Flask(__name__)
app.secret_key = config.get('SECRET_KEY')
app.config['SQLALCHEMY_DATABASE_URI'] = config.get('SQLALCHEMY_DATABASE_URI')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True

db = SQLAlchemy(app)


class User(db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    user_ID = db.Column(db.Integer)
    name = db.Column(db.String)
    kull = db.Column(db.Integer)
    activities = db.relationship('UserActivity', backref='users', lazy=False)

    def __init__(self, user_ID, name, kull):
        self.user_ID = user_ID
        self.name = name
        self.kull = kull


class UserActivity(db.Model):

    __tablename__ = "activities"

    id = db.Column(db.Integer, primary_key=True)
    aktivitetsID = db.Column(db.Integer)
    distanse = db.Column(db.Integer)
    aktivitet = db.Column(db.String(10))
    activityDate = db.Column(db.String)
    userId = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __init__(self, aktivitetsID, distanse, aktivitet, activityDate, userId):
        self.aktivitetsID = aktivitetsID
        self.distanse = distanse
        self.aktivitet = aktivitet
        self.activityDate = activityDate
        self.userId = userId


class Athletedata:
    def __init__(self, userID, activityID, typeOfActivity, distance, startDate):
        self.userID = userID
        self.activityID = activityID
        self.typeOfActivity = typeOfActivity
        self.distance = distance
        self.startDate = startDate


def poengUtregner(x):
    points = 0
    bestAthlete = {
        "name": " ",
        "poeng": 0
    }
    # Finner alle elementer i user tabellen som har kull=x
    vingX = User.query.filter_by(kull=x).all()
    for person in vingX:
        aktiviteterX = UserActivity.query.filter(UserActivity.userId==person.name).all()

        person_poeng = 0                                                        
        for akt in aktiviteterX:
            date_string = akt.activityDate
            datetime_object = datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%SZ').date()
            if(datetime_object > STARTDATE):
                
                if(akt.aktivitet == "Run"):
                    points += akt.distanse
                    person_poeng += akt.distanse
                if(akt.aktivitet == "Ride"):
                    points += akt.distanse
                    person_poeng += akt.distanse
                if(person_poeng > bestAthlete["poeng"]):
                    bestAthlete["name"] = person.name
                    bestAthlete["poeng"] = person_poeng
    return points, bestAthlete


athlete = {}
client_id = '*****'
client_secret = '*****************************'
authorization_base_url = 'https://www.strava.com/oauth/authorize'
token_url = 'https://www.strava.com/oauth/token'
api_url = 'https://www.strava.com/api/v3/athlete/activities'
redirect_uri = 'https://www.nanderson.xyz/callback'
scope = "activity:read"


@app.route("/")
def home():
    return render_template('index.html')


@app.route('/login', methods=["GET", "POST"])
def authorize():
    # Step 1: User Authorization
    if request.method == "POST":
        req = request.form
        session["kull"] = req.get("kull")

    strava = OAuth2Session(client_id, redirect_uri=redirect_uri, scope=scope)
    authorization_url, state = strava.authorization_url(authorization_base_url)
    # State is used to prevent CSRF, keep this for later.
    session['oauth_state'] = state
    return redirect(authorization_url)  # Redirecting to strava Oauth2 page


@app.route("/callback", methods=["GET"])
def callback():
    strava = OAuth2Session(client_id, state=session['oauth_state'])
    token = strava.fetch_token(token_url, include_client_id=client_id,
                               client_secret=client_secret,
                               authorization_response=request.url)
    strava = OAuth2Session(client_id, token=token)
    data = strava.get(api_url).json()
    name = strava.get("https://www.strava.com/api/v3/athlete").json()
    full_name = name["firstname"] + " " + name["lastname"]

    usr = User(data[0]["athlete"]["id"], full_name, int(session["kull"]))
    exists = db.session.query(db.session.query(
        User).filter_by(name=usr.name).exists()).scalar()
    if not exists:
        db.session.add(usr)
        db.session.commit()
    for key in range(0, len(data)):
        athlete[key] = Athletedata(userID=data[key]["athlete"]["id"],
                                   activityID=data[key]["id"],
                                   typeOfActivity=data[key]["type"],
                                   distance=data[key]["distance"],
                                   startDate=data[key]["start_date"])
    #usr = users(athlete[key].userID, athlete[key].activityID, athlete[key].distance, athlete[key].typeOfActivity)
        act = UserActivity(athlete[key].activityID,
                           athlete[key].distance,
                           athlete[key].typeOfActivity,
                           athlete[key].startDate,
                           usr.name)
        exists = db.session.query(db.session.query(UserActivity).filter_by(
            aktivitetsID=act.aktivitetsID).exists()).scalar()
        if not exists:
            db.session.add(act)
    db.session.commit()

    return redirect(url_for("profile"))


@app.route("/profile")
def profile():
    kull3 = poengUtregner(KULL3)
    kull2 = poengUtregner(KULL2)
    kull1 = poengUtregner(KULL3)
    return render_template("profile.html",
                           kull1=kull1,
                           kull2=kull2,
                           kull3=kull3)


if __name__ == "__main__":
    # This allows us to use a plain HTTP callback
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    db.create_all()
    app.secret_key = os.urandom(24)
    app.run(debug=True)
